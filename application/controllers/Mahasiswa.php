<?php 
    class Mahasiswa extends CI_Controller{
        public function __construct(){
            parent::__construct();

            $this->load->model(array('mahasiswa_m'));
        }

        public function index(){
            $data = array(
                'theme_page' => 'mahasiswa/mahasiswa',
                'judul' => 'Mahasiswa'
            );

            $this->load->view('theme/index', $data);
        }

        public function insert(){
            $data = array(
                'theme_page' => 'mahasiswa/insert_mahasiswa',
                'judul' => 'Mahasiswa'
            );

            $this->load->view('theme/index', $data);
        }

        public function insert_submit(){
            $nim = $this->input->post('nim');
            $nama = $this->input->post('nama');
            $jenkel = $this->input->post('jenkel');
            $alamat = $this->input->post('alamat');
            $no_hp = $this->input->post('no_hp');
            $agama = $this->input->post('agama');
            $status_mhs = $this->input->post('status_mhs');
            $prodi = $this->input->post('prodi');

            $data = array(
                'NIM' => $nim,
                'nama' => $nama,
                'jenis_kelamin' => $jenkel,
                'alamat' => $alamat,
                'no_telepon' => $no_hp,
                'agama' => $agama,
                'status_mahasiswa' => $status_mhs,
                'kode_prodi' => $prodi
            );
            $this->mahasiswa_m->insert($data);
            redirect('mahasiswa/insert');
        }
    }